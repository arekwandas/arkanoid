package brickBreaker;

import javax.swing.JFrame;

public class Main {

    public static void main(String[] args) {
        JFrame applicationFrame = createApplicationFrame();
        applicationFrame.add(new Gameplay());
    }

    private static JFrame createApplicationFrame() {
        JFrame applicationFrame = new JFrame();
        applicationFrame.setBounds(10, 10, 700, 600);
        applicationFrame.setTitle("Arka-noid");
        applicationFrame.setResizable(false);
        applicationFrame.setVisible(true);
        applicationFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        applicationFrame.setLocationRelativeTo(null);
        return applicationFrame;
    }

}
