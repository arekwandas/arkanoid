package brickBreaker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Gameplay extends JPanel implements KeyListener, ActionListener {

    private boolean play = false;
    private int score = 0;
    private int level = 1;
    private int totalBricks = 8;
    private Timer timer;
    private int delay = 8;
    private Random rand = new Random();
    int value = rand.nextInt(50);
    private int playerX = value * 10;
    private int ballPosX = 120;
    private int ballPosY = 450;
    private int ballXdir = 0;
    private int ballYdir = -2;
    private int row = 2;
    private int col = 4;
    private MapGenerator map;

    public Gameplay() {
        map = new MapGenerator(row, col);

        if ((value & 1) == 0) {
            ballXdir = 1;
        } else {
            ballXdir = -1;
        }

        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer = new Timer(delay, this);
        timer.start();
    }

    public void paint(Graphics graphics) {
        drawBackground(graphics);

        map.draw((Graphics2D)graphics);

        drawBorders(graphics);
        drawScores(graphics);
        drawLevelLabel(graphics);
        drawQuitLabel(graphics);
        drawPaddle(graphics);
        drawBall(graphics);

        if (ballPosY > 570) {
            endGame();
            drawFinalInfo(graphics, Color.RED, "Press ENTER to restart");
        }

        if (totalBricks < 1) {
            endGame();
            drawFinalInfo(graphics, Color.GREEN, "Press ENTER to continue");
        }

        graphics.dispose();
    }

    private void drawBackground(Graphics graphics) {
        graphics.setColor(Color.black);
        graphics.fillRect(1, 1, 692, 592);
    }

    private void drawBorders(Graphics graphics) {
        graphics.setColor(Color.yellow);
        graphics.fillRect(0, 0, 3, 592);
        graphics.fillRect(0, 0, 692, 3);
        graphics.fillRect(691, 0, 3, 592);
    }

    private void drawScores(Graphics graphics) {
        String scoreText = "Score: " + score;
        drawString(graphics, 25, Color.ORANGE, scoreText, 500, 30);
    }

    private void drawLevelLabel(Graphics graphics) {
        String levelText = "Level: " + level;
        drawString(graphics, 20, Color.ORANGE, levelText, 10, 40);
    }

    private void drawQuitLabel(Graphics graphics) {
        drawString(graphics, 14, Color.RED, "Press ESC to Quit", 10, 18);
    }

    private void drawPaddle(Graphics graphics) {
        graphics.setColor(Color.green);
        graphics.fillRect(playerX, 550, 100, 7);
    }

    private void drawBall(Graphics graphics) {
        graphics.setColor(Color.yellow);
        graphics.fillOval(ballPosX, ballPosY, 14, 14);
    }

    private void endGame() {
        play = false;
        ballXdir = 0;
        ballYdir = 0;
    }

    private void drawFinalInfo(Graphics graphics, Color color, String finalMessage) {
        String scoreText = "Game over! Your score: " + score;
        drawString(graphics, 45, color, scoreText, 90, 300);
        drawString(graphics, 35, color, finalMessage, 170, 350);
        drawString(graphics, 35, color, "Press ESCAPE to quit", 180, 400);
    }

    private void drawString(Graphics graphics, int fontSize, Color color, String text, int x, int y) {
        graphics.setColor(color);
        graphics.setFont(new Font("serif", Font.BOLD, fontSize));
        graphics.drawString(text, x, y);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        timer.start();
        if (play) {
            if (new Rectangle(ballPosX, ballPosY, 20, 20).intersects(new Rectangle(playerX, 550, 100, 8))) {
                ballYdir = -ballYdir;

            }

            A:
            for (int i = 0; i < map.map.length; i++) {
                for (int j = 0; j < map.map[0].length; j++) {
                    if (map.map[i][j] > 0) {
                        int brickX = j * map.brickWidth + 80;
                        int brickY = i * map.brickHeight + 50;
                        int brickWidth = map.brickWidth;
                        int brickHeight = map.brickHeight;

                        Rectangle rect = new Rectangle(brickX, brickY, brickWidth, brickHeight);
                        Rectangle ballRect = new Rectangle(ballPosX, ballPosY, 20, 20);
                        Rectangle brickRect = rect;

                        if (ballRect.intersects(brickRect)) {
                            map.setBrickValue(0, i, j);
                            totalBricks--;
                            score += 10;

                            if (ballPosX + 19 <= brickRect.x || ballPosX + 1 >= brickRect.x + brickRect.width) {
                                ballXdir = -ballXdir;
                            } else {
                                ballYdir = -ballYdir;
                            }
                            break A;
                        }
                    }
                }
            }

            ballPosX += ballXdir;
            ballPosY += ballYdir;
            if (ballPosX < 0) {
                ballXdir = -ballXdir;
            }
            if (ballPosY < 0) {
                ballYdir = -ballYdir;
            }
            if (ballPosX > 670) {
                ballXdir = -ballXdir;
            }
        }

        repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (playerX >= 600) {
                playerX = 600;
            } else {
                moveRight();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            if (playerX <= 0) {
                playerX = 0;
            } else {
                moveLeft();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (!play) {                            //if ball falls off the map
                if (totalBricks > 0) {
                    play = true;
                    Random rand = new Random();
                    int value = rand.nextInt(50);
                    ballPosX = value * 10;
                    ballPosY = 450;
                    if ((value & 1) == 0) {
                        ballXdir = 1;
                    } else {
                        ballXdir = -1;
                    }
                    ballYdir = -2;
                    playerX = 310;
                    score = 0;
                    totalBricks = 8;
                    map = new MapGenerator(2, 4);

                    repaint();
                } else {                            //if all bricks destroyed

                    play = true;
                    Random rand = new Random();
                    int value = rand.nextInt(50);
                    ballPosX = value * 10;
                    ballPosY = 450;
                    if ((value & 1) == 0) {
                        ballXdir = 1;
                    } else {
                        ballXdir = -1;
                    }
                    ballYdir = -2;
                    playerX = 310;
                    row = row + 1;
                    col = col + 1;
                    level = level + 1;
                    map = new MapGenerator(row, col);
                    totalBricks = row * col;

                    //increase ball speed
                /*    delay = delay-(level/10000);
                    timer = new Timer(delay, this);
                    timer.start();  */
                    repaint();
                }
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            System.exit(0);
        }
    }

    public void moveRight() {
        play = true;
        playerX += (20 + level);
    }

    public void moveLeft() {
        play = true;
        playerX -= (20 + level);
    }

}
